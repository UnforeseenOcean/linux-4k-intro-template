
#ifndef DBG_H_
#define DBG_H_

#if defined(VERBOSE) && !defined(NOSTDLIB)
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

#include "def.h"

__attribute__((__format__(printf, 1, 2)))
static void k2dbg_base(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vdprintf(STDERR_FILENO, fmt, args);
    va_end(args);
}

#define k2dbg(fmt, ...) k2dbg_base("[" __FILE__ ":%s:" STRINGIFY(__LINE__) "]\t" fmt, __func__, ##__VA_ARGS__)
#define k2pingimp(msg) k2dbg("%s", msg)
#define k2ping(msg) k2dbg("%s", msg)
#else
#define k2dbg(fmt, ...) do{}while(0)

#ifdef DESPERATE_UNSAFE
#define k2pingimp(msg) do{}while(0)
#define k2ping(msg) do{}while(0)
#else

#include "sys.h"
#include "intr.h"

#define k2pingimp(msg) do{\
    SYS_write(STDERR_FILENO, msg, \
        (sizeof(msg)/sizeof(*msg)>(sizeof(void*)/sizeof(*msg)))\
            ?(sizeof(msg)-1)\
            :INTR_strlen(msg));\
}while(0)\

#ifdef DESPERATE
#define k2ping(msg) do{}while(0)
#else
#define k2ping(msg) k2pingimp(msg)
#endif

#endif /* DESPERATE_UNSAFE */
#endif /* VERBOSE */

#endif

