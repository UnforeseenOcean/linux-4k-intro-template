
#ifndef INTR_H_
#define INTR_H_

#include "def.h"

#if (!defined(__x86_64__) && !defined(__i386__)) || (!defined(__linux__))
#error "This demo won't work on your platform! Linux on i386 or x86_64 is required!"
#endif

force_inline static size_t INTR_strlen(const char* s) {
    size_t len;
#ifdef __x86_64__
    asm volatile("xorq %%rax, %%rax\n"
                 "xorq %%rcx, %%rcx\n"
                 "dec %%rcx\n"
                 "repne scasb\n"
                 "neg %%rcx\n"
                 "sub $2, %%rcx\n"
                 :"=c"(len)
                 :"D"(s)
                 :"rax");
#elif defined(__i386__)
    asm volatile("xorl %%eax, %%eax\n"
                 "xorl %%ecx, %%ecx\n"
                 "dec %%ecx\n"
                 "repne scasb\n"
                 "neg %%ecx\n"
                 "sub $2, %%ecx\n"
                 :"=c"(len)
                 :"D"(s)
                 :"eax");
#endif

    return len;
}

force_inline static void INTR_memcpy(void* dest, const void* src, size_t sz) {
    asm volatile("rep movsb\n"::"S"(src),"D"(dest),"c"(sz):"memory");
}

force_inline static void INTR_memset(void* dest, int val, size_t sz) {
    asm volatile("rep stosb\n"::"a"(val),"D"(dest),"c"(sz):"memory");
}

#endif

