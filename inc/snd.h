
#ifndef SND_H_
#define SND_H_

#include <stdbool.h>
#include <stddef.h>

#include "def.h"

lto_inline void snd_fork_addr(const void* sndelf, size_t elfsz);

lto_inline bool snd_wait_sync(void);

lto_inline void snd_kill(void);

#endif

