
#ifndef DEMO_H_
#define DEMO_H_

#include "def.h"

typedef float demotime_t;

lto_inline extern void effect_init(void);
lto_inline extern void effect_tick(demotime_t t, int w, int h);
#ifndef DESPERATE
lto_inline extern void effect_kill(void);
#endif

#endif

