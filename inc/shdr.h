
#ifndef SHDR_H_
#define SHDR_H_

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>

#include "def.h"

lto_inline_maybe k2lib_func GLuint shdr_mk(const char* src, GLenum typ);

force_inline static GLuint shdr_mk_frag(const char* src) {
    return shdr_mk(src, GL_FRAGMENT_SHADER);
}
force_inline static GLuint shdr_mk_vert(const char* src) {
    return shdr_mk(src, GL_VERTEX_SHADER);
}
force_inline static GLuint shdr_mk_geom(const char* src) {
    return shdr_mk(src, GL_GEOMETRY_SHADER);
}

lto_inline_maybe k2lib_func GLuint prgm_mk1(GLuint shdr);
lto_inline_maybe k2lib_func GLuint prgm_mk2(GLuint shdra, GLuint shdrb);
lto_inline_maybe k2lib_func GLuint prgm_mk3(GLuint shdra, GLuint shdrb, GLuint shdrc);

lto_inline k2lib_func GLuint shdrprgm_mk_frag(const char* src);

#endif

