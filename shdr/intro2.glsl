#version 330

out vec4 C;
uniform float T;
uniform sampler2D tex0;

vec2 hash22(vec2 p) {
    vec3 p3 = fract(vec3(p.xyx) * 443.8975);
    p3 += dot(p3, p3.yzx+19.19);
    return fract(vec2((p3.x + p3.y)*p3.z, (p3.x+p3.z)*p3.y));
}
float vn(float t, float s) {
    int it = int(t);
    float ft = t-it;
    return mix(hash22(vec2(it, s)).x, hash22(vec2(it+1, s)).x, smoothstep(0.0, 1.0, ft));
}


vec4 saturate(vec4 x)  {
    return clamp(x, vec4(0.0), vec4(1.0));

}

vec4 ac(float f) {
    f = f * 3.0 - 1.5;
    return saturate(vec4(-f, 1.0 - abs(f), f, 1.0));
}



void main() {
    float INTRO_DURATION = 120;
    vec4 f = vec4(0.0);
    for (int k = 0; k < 16; ++k) {
        float tk = T + (k / 16.0)/10.0;
        float tn = hash22(vec2(tk)).x;
        float scale = mix(0.3 + vn(tk*0.12, 0.0), 1.0, smoothstep(INTRO_DURATION-4,INTRO_DURATION, T));

        vec2 tr = vec2(vn(132.0+tk*0.1, 432.0), vn(12.43+tk*0.1, 0.0))*1200.0;
        vec2 sc = vec2(960, 540);
        ivec2 ts = ivec2(64,128);
        int x = int(floor((tr.x+sc.x + scale * (gl_FragCoord.x-sc.x-tr.x)) / ts.x));
        int y = int(floor((tr.y+sc.y + scale * (gl_FragCoord.y-sc.y-tr.y)) / ts.y));

        int ti = int(mod(vn(tk*3.34123, x+y*100)*20.0, 128.0));
        if (T > INTRO_DURATION) {
            ti = x+y*16;
            if (x>15 || y>7) {
                ti = -1;
            }
        }

        vec2 n = ((gl_FragCoord.xy - sc - tr) * scale + tr + sc) / ts;
        n = mod(n, vec2(1));

        vec2 c = vec2(0.5);
        if (vn(tk, x*y) > 0.9 && T < INTRO_DURATION) {
            n = c;
        }

        n = clamp(n, vec2(0.0), vec2(1.0));
        vec2 tc = (n+vec2(ti%16, ti/16)) * vec2(1.0/16, 1.0/8);
        tc.y = 1.0-tc.y;
        vec4 tc0 = ti>=0? texture(tex0, tc) : vec4(0.0);

        float l = length((n-c)*vec2(1.0, 2.0));
        f += ac(k/16.0)*vec4(tc0.a)/16.0;
    }
    C = vec4(pow(f.rgb* smoothstep(0.0, 30.0, T), vec3(1.0/2.2)),1.0);
}
