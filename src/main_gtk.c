
#ifdef BACKEND_GTK

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/glx.h>
#include <GL/glext.h>

#include "def.h"
#include "sys.h"
#include "dbg.h"
#include "intr.h"
#include "loop.h"

static void quitme(void) { gtk_main_quit(); }

#ifndef DESPERATE
static gboolean on_esc(GtkWidget* wnd, GdkEventKey* ev) {
	if (ev->keyval == GDK_KEY_Escape) gtk_main_quit();

	return FALSE;
}
#endif

static gboolean on_render(GtkGLArea* gla, GdkGLContext* ctx) {
#if defined(CANVAS_RESIZABLE) && !defined(MODE_DUMP)
	loop_tick(gtk_widget_get_allocated_width ((GtkWidget*)gla),
			  gtk_widget_get_allocated_height((GtkWidget*)gla));
#else
	loop_tick(CANVAS_WIDTH, CANVAS_HEIGHT);
#endif

	gtk_gl_area_queue_render(gla);

#ifndef DESPERATE
	while (gtk_events_pending()) gtk_main_iteration();
#endif

	return TRUE;
}

static void on_realize(GtkGLArea* gla) {
	gtk_gl_area_make_current(gla);

#ifndef NO_ERROR_CHECKING
	if (!loop_init()) {
		k2ping("no init\n");
#ifndef DESPERATE_UNSAFE
		gtk_main_quit();
#else
		SYS_break();
#endif
	}
#else
	loop_init();
#endif
}

__attribute__((__used__, __externally_visible__))
int main(int argc, char* argv[]
#ifdef NEED_ENVIRON
        , char* envp[]
#endif
        ) {
	gtk_init(NULL, DONT_CARE(char***));
	//((void (*)(int*))gtk_init)(NULL);

	GtkWidget* wnd = gtk_window_new(GTK_WINDOW_TOPLEVEL),
	         * gla = gtk_gl_area_new();
	gtk_container_add(/*GTK_CONTAINER(wnd)*/(GtkContainer*)wnd, gla);

	g_signal_connect(wnd, "destroy", gtk_main_quit, NULL);
#ifndef DESPERATE
	g_signal_connect(wnd, "key_press_event", on_esc, NULL);
#endif
	g_signal_connect(gla, "realize", on_realize, NULL);
	g_signal_connect(gla, "render" , on_render , NULL);

	gtk_widget_show_all(wnd);
#ifdef FULLSCREEN
	gtk_window_fullscreen((GtkWindow*)wnd);
#endif
#ifdef HIDEMOUSE
	gdk_window_set_cursor(gtk_widget_get_window(wnd),
			gdk_cursor_new(GDK_BLANK_CURSOR));
#endif

	gtk_main();

#ifndef DESPERATE_UNSAFE
	loop_exit();
#endif

#ifndef DESPERATE_UNSAFE
	return 0;
#else
	return DONT_CARE(int);
#endif
}

#endif

