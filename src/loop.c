
#include <stdint.h>
#include <stdbool.h>

#include <errno.h>

#ifdef BACKEND_SDL2
#include <SDL_timer.h>
#endif

#include "sys.h"
#include "dbg.h"
#include "snd.h"
#include "file.h"

#include "loop.h"

#include "snd.bin.h"

#ifdef MODE_DUMP
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>
#endif

#include "demo.h"

#ifdef MODE_DUMP
static struct { uint8_t r, g, b; } backbuf[CANVAS_WIDTH*CANVAS_HEIGHT];
static uint32_t framecnt;
#else
#ifdef BACKEND_SDL2
static uint32_t start;
#else
static struct timespec start;
#endif /* backends */
#endif /* !MODE_DUMP */

force_inline static demotime_t ts2time(struct timespec ts) {
	return (demotime_t)ts.tv_sec + (demotime_t)ts.tv_nsec * (demotime_t)(1.0/(1000*1000*1000));
}
force_inline static demotime_t ms2time(uint32_t ms) {
	return (demotime_t)ms * (demotime_t)(1.0/1000);
}

bool loop_init() {
#ifdef MODE_DUMP
	framecnt = 0;
#endif

#ifndef NO_AUDIO
	struct file_res f = file_read(snd);
	snd_fork_addr(f.data, f.len);
#endif /* NO_AUDIO */

	effect_init();

#ifndef NO_AUDIO
	// this ensures the visuals won't start before the music has started, but
	// not that the music won't start before the visuals have started...
	snd_wait_sync();
#endif

#ifdef MODE_DUMP
	framecnt = 0;
#else /* MODE_REL */
#ifdef BACKEND_SDL2
	start = SDL_GetTicks();
#else
	SYS_clock_gettime(CLOCK_REALTIME, &start);
#endif /* backends */
#endif /* MODE_* */

	return true;
}

void loop_tick(int w, int h) {
	demotime_t t;

#ifdef MODE_DUMP
	t = (demotime_t)framecnt * (demotime_t)(1.0/60);
	++framecnt;
#else
#ifdef BACKEND_SDL2
	t = ms2time(SDL_GetTicks() - start);
#else
	struct timespec now;
	SYS_clock_gettime(CLOCK_REALTIME, &now);
	now.tv_nsec -= start.tv_nsec;
	now.tv_sec  -= start.tv_sec ;
	// handled properly by ts2time
	/*if (now.tv_nsec < 0) {
		--now.tv_sec;
		now.tv_nsec += 1000 * 1000 * 1000;
	}*/

	t = ts2time(now);
#endif

#endif /* MODE_DUMP */

	effect_tick(t, w, h);

#ifdef MODE_DUMP
	glReadBuffer(GL_BACK);
	glReadPixels(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, backbuf);
	SYS_write(STDOUT_FILENO, backbuf, sizeof(backbuf));
#endif
}

void loop_exit(void) {
	snd_kill();
}

