
#include <stdint.h>
#include <stdio.h>

#include "dbg.h"
#include "sys.h"

#include "snd.h"

#ifndef DESPERATE_UNSAFE
static pid_t childpid;
#endif

static const char* emptystrarr[] = { NULL };

void snd_fork_file(const char* path) {
#ifndef MODE_DUMP
    volatile int syncpipe[2], syncpip2[2];
#ifndef WEAK_VA_SYNC
    SYS_pipe(syncpipe);
    // *sometimes*, the syncpipe vars are put at the top of the stack
    // (apparently only when using incremental linking), and SYS_pipe writes
    // to the stack (to load the syscall num into rax), so the values get
    // garbled... this should fix it
    register int readend = syncpipe[0], writeend = syncpipe[1];
#endif
#ifndef WEAK_AV_SYNC
    SYS_pipe(syncpip2);
    register int readen2 = syncpip2[0], writeen2 = syncpip2[1];
#endif
#endif /* MODE_DUMP */

    // fyi, when it bugs out, various other numbers (syscall num, etc.)
    // get interpreted as file descriptors, so eg. stdin will get closed,
    // which will cause *weird* stuff (can't read sync, which leads to bad sync,
    // and ultimately to xcb segfaulting... somehow)

    if (!(
#ifndef DESPERATE_UNSAFE
            childpid =
#endif
            SYS_fork())) {
        // child

#ifndef MODE_DUMP
        // yes, these preprocessor directives could be merged, but if it's done
        // in this order, the code compresses better...
#ifndef WEAK_VA_SYNC
#ifndef DESPERATE
        SYS_close( readend); // close read end
#endif
#endif
#ifndef WEAK_AV_SYNC
#ifndef DESPERATE
        SYS_close(writeen2);
#endif
#endif
#ifndef WEAK_VA_SYNC
        SYS_dup2(writeend, STDOUT_FILENO);
#endif
#ifndef WEAK_AV_SYNC
        SYS_dup2( readen2, STDIN_FILENO );
#endif
#endif /* MODE_DUMP */

        SYS_execve(path, emptystrarr, emptystrarr);
    } else {
        // parent

#ifndef MODE_DUMP
#ifndef WEAK_VA_SYNC
        SYS_close(writeend); // close write end
#endif
#ifndef WEAK_AV_SYNC
        SYS_close( readen2);
#endif
#ifndef WEAK_VA_SYNC
        SYS_dup2( readend, STDIN_FILENO );
#endif
#ifndef WEAK_AV_SYNC
        SYS_dup2(writeen2, STDOUT_FILENO);
#endif
#endif /* MODE_DUMP */
    }
}

void snd_fork_addr(const void* sndelf, size_t elfsz) {
#ifndef MODE_DUMP
    volatile int syncpipe[2], syncpip2[2];
#ifndef WEAK_VA_SYNC
    SYS_pipe(syncpipe);
    register int readend = syncpipe[0], writeend = syncpipe[1];
#endif
#ifndef WEAK_AV_SYNC
    SYS_pipe(syncpip2);
    register int readen2 = syncpip2[0], writeen2 = syncpip2[1];
#endif
#endif /* MODE_DUMP */

    if (!(
#ifndef DESPERATE_UNSAFE
            childpid =
#endif
            SYS_fork())) {
        // child
#ifndef MODE_DUMP
#ifndef DESPERATE
#ifndef WEAK_VA_SYNC
        SYS_close( readend); // close read end
#endif
#ifndef WEAK_AV_SYNC
        SYS_close(writeen2);
#endif
#endif
#ifndef WEAK_VA_SYNC
        SYS_dup2(writeend, STDOUT_FILENO);
#endif
#ifndef WEAK_AV_SYNC
        SYS_dup2( readen2, STDIN_FILENO );
#endif
#endif /* MODE_DUMP */

        register void* name;
#ifdef __x86_64__
        asm volatile("mov %%rsp, %[name]\n":[name]"=r"(name)::);
#elif defined(__i386__)
        asm volatile("mov %%esp, %[name]\n":[name]"=r"(name)::);
#else /* ARM */
        asm volatile("mov %[name], sp\n":[name]"=r"(name)::);
#endif
        int fd = SYS_memfd_create(name, 0);
        SYS_write(fd, sndelf, elfsz);
        SYS_lseek(fd, 0, SEEK_SET);
        SYS_execveat(fd, "", emptystrarr, emptystrarr, AT_EMPTY_PATH);
    } else {
        // parent
#ifndef MODE_DUMP
#ifndef WEAK_VA_SYNC
#ifndef DESPERATE
        SYS_close(writeend); // close write end
#endif
#endif
#ifndef WEAK_AV_SYNC
#ifndef DESPERATE
        SYS_close( readen2);
#endif
#endif
#ifndef WEAK_VA_SYNC
        SYS_dup2( readend, STDIN_FILENO );
#endif
#ifndef WEAK_AV_SYNC
        SYS_dup2(writeen2, STDOUT_FILENO);
#endif
#endif /* MODE_DUMP */
    }
}

bool snd_wait_sync(void) {
#ifndef MODE_DUMP
    uint32_t blah[PIPE_WILL_BLOCK_BUF/sizeof(uint32_t)];
#ifndef WEAK_VA_SYNC
    SYS_read (STDIN_FILENO, &blah, PIPE_WILL_BLOCK_BUF);
#endif
#ifndef WEAK_AV_SYNC
    SYS_write(STDOUT_FILENO, (const void*)&blah, PIPE_WILL_BLOCK_BUF);
#endif
    return true;
#endif /* MODE_DUMP */
}

void snd_kill(void) {
#ifndef DESPERATE_UNSAFE
    if (childpid) SYS_kill(childpid, SIGTERM);
#endif
}

