
#if defined(BACKEND_SDL2) && defined(__x86_64__) && defined(USE_INLINE_ASM_STUFF) && !defined(MODE_DUMP)

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include <SDL.h>
#include <SDL_video.h>
#include <SDL_events.h>
#include <SDL_keyboard.h>
#include <SDL_mouse.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/glx.h>
#include <GL/glext.h>

#include "sys.h"
#include "dbg.h"
#include "intr.h"
#include "loop.h"

#include "gldbg.c"

const char windowname[] = "";

__attribute__((__used__, __externally_visible__
#ifndef __clang__
	,__naked__
#endif
))
int main(int argc, char* argv[]
#ifdef NEED_ENVIRON
        , char* envp[]
#endif
        ) {
	// args: rdi, rsi, rdx, rcx, r8, r9
	// preserve: rbx, rsp, rbp, r12, r13, r14, and r15
	// scratch: rax, rdi, rsi, rdx, rcx, r8, r9, r10, r11

	register SDL_Window* w;
	asm volatile(
		"push $0x20\n" // SDL_INIT_VIDEO
		"push $0x20\n" // align
		"pop %%rdi\n"
		"call *SDL_Init@GOTPCREL(%%rip)\n"
#ifndef NO_ERROR_CHECKING
		"test %%eax, %%eax; jz 1f; ret;\n1:\t"
#endif
		//"push $0\n" // alignment
		"push $windowname; push $0; push $0\n"
		"push $" STRINGIFY(CANVAS_WIDTH) "; push $" STRINGIFY(CANVAS_HEIGHT) "\n"
		"push $(2" // opengl window
#ifndef DESPERATE_UNSAFE
			"|0x2000" // allow highdpi
#endif
#ifdef FULLSCREEN
			"|1" // fullscreen
#elif defined(CANVAS_RESIZABLE)
			"|0x20" // resizable
#endif
		"); pop %%r9; pop %%r8; pop %%rcx; pop %%rdx; pop %%rsi; pop %%rdi\n"
		"call *SDL_CreateWindow@GOTPCREL(%%rip)\n"
		//"pop %%rdi\n"
#ifndef NO_ERROR_CHECKING
		"test %%al, %%al; jz 1f; int3;\n1:\t"
#endif
		"xchg %%rax, %%rbx\n"
#ifndef DESPERATE_UNSAFE
#ifdef USE_CORE_PROFILE
		"push $1; push $21\n" // core
#else
		"push $2; push $21\n" // compat
#endif
		"push $3; push $18\n" // min ver
		"push $4; push $17\n" // maj ver
		"pop %%rdi; pop %%rsi\n"
		"call *SDL_GL_SetAttribute@GOTPCREL(%%rip)\n"
		"pop %%rdi; pop %%rsi\n"
		"call *SDL_GL_SetAttribute@GOTPCREL(%%rip)\n"
		"pop %%rdi; pop %%rsi\n"
		"call *SDL_GL_SetAttribute@GOTPCREL(%%rip)\n"
#endif
		"push %%rbx; push %%rbx\n"
		"pop %[w]; pop %%rdi\n"
		"call *SDL_GL_CreateContext@GOTPCREL(%%rip)\n"
#ifndef NO_ERROR_CHECKING
		"test %%eax, %%eax; jz 1f; int3;\n1:\t"
#endif
#ifdef HIDEMOUSE
		"push $0; pop %%rdi\n"
		"call *SDL_ShowCursor@GOTPCREL(%%rip)\n"
#endif
#ifdef VSYNC
		"push $1; pop %%rdi\n"
		"call *SDL_GL_SetSwapInterval@GOTPCREL(%%rip)\n"
#endif
	:[w]"=r"(w)::"rbx","rdi","rsi","rdx","rcx","r8","r9","r10","r11");
#ifdef VERBOSE
	glEnable(GL_DEBUG_OUTPUT|GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(gldbg_cb, NULL);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE,
			0, NULL, GL_TRUE);

	k2dbg("GL v%s/glsl v%s (%s: %s)\n", glGetString(GL_VERSION),
			glGetString(GL_SHADING_LANGUAGE_VERSION),
			glGetString(GL_VENDOR), glGetString(GL_RENDERER));
	k2dbg("GL exts: %s\n", glGetString(GL_EXTENSIONS));
#endif

#ifdef NO_ERROR_CHECKING
	loop_init();
#else
	if (!loop_init()) {
		k2ping("no init\n");
		goto END;
	}
#endif

#if defined(CANVAS_RESIZABLE) && !defined(MODE_DUMP)
	register int ww asm("r12") = CANVAS_WIDTH ,
				 hh asm("r13") = CANVAS_HEIGHT;
#else
	int ww = CANVAS_WIDTH, hh = CANVAS_HEIGHT;
#endif
	SDL_Event e;
	do {
	NEXT:
#if defined(CANVAS_RESIZABLE) && !defined(MODE_DUMP)
		asm volatile(
			"mov %%rsp, %%rdx; push %%rsp\n"
			"mov %%rsp, %%rsi; push %%rsp\n"
			"push %[w]; pop %%rdi\n"
			"call *SDL_GL_GetDrawableSize@GOTPCREL(%%rip)\n"
			"pop %%r12; pop %%r13\n"
		::[w]"r"(w):"rax","rdi","rsi","rdx","rcx","r8","r9","r10","r11");
#endif
		loop_tick(ww, hh);
		asm volatile goto(
			"push %[e]\n" // alignment
			"push %[e]\n"
			"push %[w]; pop %%rdi\n"
			"call *SDL_GL_SwapWindow@GOTPCREL(%%rip)\n"
			"1: pop %%rdi; pop %%rdi\n"
			"call *SDL_PollEvent@GOTPCREL(%%rip)\n"
			"test %%eax, %%eax; jz %l[NEXT]\n"
			"cmpl $0x100, (%[e])\n"
			"je %l[END]\n"
			"push %[e]; push %[e]; jmp 1b\n"
		::[e]"r"(&e),[w]"p"(w):"rax","rdi","rsi","rdx","rcx","r8","r9","r10","r11":END,NEXT);
	} while (true);
END:
#ifndef DESPERATE_UNSAFE
	loop_exit();
#endif
	SYS_exit_group(DONT_CARE(int));//return DONT_CARE(int);
}

#endif

