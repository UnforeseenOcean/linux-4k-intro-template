
#include <stdbool.h>

#include "dbg.h"

#include "shdr.h"

GLuint shdr_mk(const char* src, GLuint typ) {
	GLuint shdr = glCreateShader(typ);

	glShaderSource(shdr, 1, &src, 0);

	glCompileShader(shdr);

#ifndef NO_ERROR_CHECKING
	{
		GLint succ;
		glGetShaderiv(shdr, GL_COMPILE_STATUS, &succ);
		if (!succ) {
#ifdef VERBOSE
			GLint len;
			glGetShaderiv(shdr, GL_INFO_LOG_LENGTH, &len);
			char log[len+1];
			glGetShaderInfoLog(shdr, len, &len, log);
			log[len] = 0;

			k2dbg("shader compilation failed:\n%s\n", log);
#endif

			return 0;
		}
	}
#endif

	return shdr;
}

//inline
static bool prgm_status(GLuint prgm) {
#ifndef NO_ERROR_CHECKING
	{
		GLint succ;
		glGetProgramiv(prgm, GL_LINK_STATUS, &succ);
		if (!succ) {
#ifdef VERBOSE
			GLint len;
			glGetProgramiv(prgm, GL_INFO_LOG_LENGTH, &len);
			char log[len+1];
			glGetProgramInfoLog(prgm, len, &len, log);
			log[len] = 0;

			k2dbg("shader program linking failed:\n%s\n", log);
#endif
			return true;
		}
	}
#endif
	return false;
}

// let LTO decide when to inline this and when not (i.e. inline <=> >1 of the
// prgm_mk* functions is used)
//inline
static GLuint prgm_check(GLuint prgm) {
	glLinkProgram(prgm);

	if (prgm_status(prgm)) return 0;

	return prgm;
}

GLuint prgm_mk1(GLuint shdra) {
	GLuint prgm = glCreateProgram();

	glAttachShader(prgm, shdra);

	return prgm_check(prgm);
}
GLuint prgm_mk2(GLuint shdra, GLuint shdrb) {
	GLuint prgm = glCreateProgram();

	glAttachShader(prgm, shdra);
	glAttachShader(prgm, shdrb);

	return prgm_check(prgm);
}
GLuint prgm_mk3(GLuint shdra, GLuint shdrb, GLuint shdrc) {
	GLuint prgm = glCreateProgram();

	glAttachShader(prgm, shdra);
	glAttachShader(prgm, shdrb);
	glAttachShader(prgm, shdrc);

	return prgm_check(prgm);
}

GLuint shdrprgm_mk_frag(const char* src) {
#if defined(__x86_64__) && defined(USE_INLINE_ASM_STUFF) && !defined(MODE_DUMP)
	register GLuint prgm asm("rax");
	asm volatile(
		"push $0; push %[src]; push $1; push $" STRINGIFY(GL_FRAGMENT_SHADER) "\n"
		"pop %%rdi; pop %%rsi; mov %%rsp, %%rdx\n"
		"call *glCreateShaderProgramv@GOTPCREL(%%rip)\n"
		"pop %%rdi; pop %%rsi\n"
	:"=a"(prgm)
	:[src]"p"(src)
	:"rdi", "rsi", "rdx", "rcx", "r8", "r9", "r10", "r11");
#else
	GLuint prgm = glCreateShaderProgramv(GL_FRAGMENT_SHADER, 1, &src);
#endif
#ifndef NO_ERROR_CHECKING
	if (prgm_status(prgm)) return 0;
#endif
	return prgm;
}

