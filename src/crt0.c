
#include "def.h"

#ifdef CUSTOM_CRT0

#include "sys.h"

#if defined(NEED_ENVIRON) && !defined(USE_LIBC_START_MAIN)
extern char** environ;
char** environ;
#endif

#ifdef NEED_AUXV
#include <sys/auxv.h>

static size_t* auxv;

size_t getauxval(size_t type) {
    register size_t* aux = auxv;
    do {
        register size_t x = *aux;
        ++aux;
        if (!x) return x;
        if (x == type) return *aux;
        ++aux;
    } while (1);
}
#endif

force_inline // wishful thinking
extern int main(int argc, char* argv[]
#ifdef NEED_ENVIRON
        , char* envp[]
#endif
    );

#ifdef USE_LIBC_START_MAIN
extern void __libc_start_main(int (*main)(int, char**, char**),
        int argc, char** argv,
        void (*init)(void), void (*fini)(void),
        void (*rtld_fini)(void),
        void* stack) __attribute__((__noplt__,__noreturn__));
#endif

#ifdef __clang__
__attribute__((__noreturn__))
extern void _start(void); // sigh
#endif
__attribute__((__used__, __noreturn__, __externally_visible__
	,__section__(".text.startup._start")
#ifndef __clang__
    ,__naked__
#endif
))
void _start() {
/*
 * https://lwn.net/Articles/630727/
 * https://lwn.net/Articles/631631/
    ------------------------------------------------------------- 0x7fff6c845000
     0x7fff6c844ff8: 0x0000000000000000
            _  4fec: './stackdump\0'                      <------+
      env  /   4fe2: 'ENVVAR2=2\0'                               |    <----+
           \_  4fd8: 'ENVVAR1=1\0'                               |   <---+ |
           /   4fd4: 'two\0'                                     |       | |     <----+
     args |    4fd0: 'one\0'                                     |       | |    <---+ |
           \_  4fcb: 'zero\0'                                    |       | |   <--+ | |
               3020: random gap padded to 16B boundary           |       | |      | | |
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|       | |      | | |
               3019: 'x86_64\0'                        <-+       |       | |      | | |
     auxv      3009: random data: ed99b6...2adcc7        | <-+   |       | |      | | |
     data      3000: zero padding to align stack         |   |   |       | |      | | |
    . . . . . . . . . . . . . . . . . . . . . . . . . . .|. .|. .|       | |      | | |
               2ff0: AT_NULL(0)=0                        |   |   |       | |      | | |
               2fe0: AT_PLATFORM(15)=0x7fff6c843019    --+   |   |       | |      | | |
               2fd0: AT_EXECFN(31)=0x7fff6c844fec      ------|---+       | |      | | |
               2fc0: AT_RANDOM(25)=0x7fff6c843009      ------+           | |      | | |
      ELF      2fb0: AT_SECURE(23)=0                                     | |      | | |
    auxiliary  2fa0: AT_EGID(14)=1000                                    | |      | | |
     vector:   2f90: AT_GID(13)=1000                                     | |      | | |
    (id,val)   2f80: AT_EUID(12)=1000                                    | |      | | |
      pairs    2f70: AT_UID(11)=1000                                     | |      | | |
               2f60: AT_ENTRY(9)=0x4010c0                                | |      | | |
               2f50: AT_FLAGS(8)=0                                       | |      | | |
               2f40: AT_BASE(7)=0x7ff6c1122000                           | |      | | |
               2f30: AT_PHNUM(5)=9                                       | |      | | |
               2f20: AT_PHENT(4)=56                                      | |      | | |
               2f10: AT_PHDR(3)=0x400040                                 | |      | | |
               2f00: AT_CLKTCK(17)=100                                   | |      | | |
               2ef0: AT_PAGESZ(6)=4096                                   | |      | | |
               2ee0: AT_HWCAP(16)=0xbfebfbff                             | |      | | |
               2ed0: AT_SYSINFO_EHDR(33)=0x7fff6c86b000                  | |      | | |
    . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .        | |      | | |
               2ec8: environ[2]=(nil)                                    | |      | | |
               2ec0: environ[1]=0x7fff6c844fe2         ------------------|-+      | | |
               2eb8: environ[0]=0x7fff6c844fd8         ------------------+        | | |
               2eb0: argv[3]=(nil)                                                | | |
               2ea8: argv[2]=0x7fff6c844fd4            ---------------------------|-|-+
               2ea0: argv[1]=0x7fff6c844fd0            ---------------------------|-+
               2e98: argv[0]=0x7fff6c844fcb            ---------------------------+
     0x7fff6c842e90: argc=3
 */

    //asm volatile(".Lloopme:\njmp .Lloopme\n");

    register ssize_t argc;
    register char** argv;
    register char** env_;
#ifdef NEED_AUXV
    register void* auxv_;
#endif

/*mov argc, [sp]
  lea argv, [sp+sizeof(size_t)]
  lea environ, [sp+argc+sizeof(size_t)]
  mov auxv, environ
  .loop:
  mov tmp, [auxv]
  add auxv, sizeof(size_t)
  cmp tmp, 0
  jne .loop*/

#ifdef __x86_64__
    asm volatile(
#ifdef __clang__
                 "pop %%rbp\n" /* workaround for lacking __naked__ support */
#endif
#ifdef USE_LIBC_START_MAIN /* => we can manually align the stack afterwards in
                              the next asm block (where it compresses better) */
                 "pop %[argc]\n"
                 "mov %%rsp, %[argv]\n"
#else
                 "pop %[argc]\n"
                 "push %[argc]\n" /* align stack */
                 "mov %%rsp, %[argv]\n"
#ifdef NEED_ENVIRON
                 "lea 8(%%rsp,%[argc],8), %[environ]\n"
#endif
#endif /* USE_LIBC_START_MAIN */
#ifdef NEED_AUXV
                 "mov %[environ], %[auxv]\n"
            ".Lloop:\n"
                 "mov (%[auxv]), %%rax\n"
                 "add $8, %[auxv]\n"
                 "test %%rax, %%rax\n"
                 "jne .Lloop\n"
#endif /* NEED_AUXV */
        :[argc]"=b"(argc),[argv]"=d"(argv)
#ifdef NEED_ENVIRON
            ,[environ]"=c"(env_)
#endif
#ifdef NEED_AUXV
            ,[auxv]"=S"(auxv_)
#endif
        ::"rax");
#elif defined(__i386__)
    asm volatile(
#ifdef __clang__
                 "pop %%ebp\n" /* workaround for lacking __naked__ support */
#endif
                 "pop %[argc]\n"
                 "mov %%esp, %[argv]\n"
#if defined(NEED_ENVIRON) && !defined(USE_LIBC_START_MAIN)
                 "lea 4(%%esp,%[argc],4), %[environ]\n"
#endif
#ifdef NEED_AUXV
                 "mov %[environ], %[auxv]\n"
            ".Lloop:\n"
                 "mov (%[auxv]), %%eax\n"
                 "add $4, %[auxv]\n"
                 "test %%eax, %%eax\n"
                 "jne .Lloop\n"
#endif
                 "push %[argc]\n" /* align stack */
                 "push %[argc]\n" /* align stack */
                 "push %[argc]\n" /* align stack */
                 "push %[argc]\n" /* align stack */
        :[argc]"=b"(argc),[argv]"=D"(argv)
#ifdef NEED_ENVIRON
            ,[environ]"=c"(env_)
#endif
#ifdef NEED_AUXV
            ,[auxv]"=d"(auxv_)
#endif
        ::"eax");
#else /* ARM */
    asm volatile("pop {%[argc]}\n"
                 "mov %[argv], sp\n"
#if defined(NEED_ENVIRON) && !defined(USE_LIBC_START_MAIN)
                 "add %[environ], sp, %[argc], lsl #2\n"
                 "add %[environ], #4" /* skip nullptr */
#endif
#ifdef NEED_AUXV
                 "mov %[auxv], %[environ]\n"
            ".Lloop:\n"
                 "ldmia %[auxv]!, {r0}\n"
                 "tst r0, r0\n"
                 "bne .Lloop\n"
#endif
        :[argc]"=l"(argc),[argv]"=l"(argv)
#ifdef NEED_ENVIRON
            ,[environ]"=l"(env_)
#endif
#ifdef NEED_AUXV
            ,[auxv]"=l"(auxv_)
#endif
        :
        :"r0","cc");
#endif

#if defined(NEED_ENVIRON) && !defined(USE_LIBC_START_MAIN)
    environ = env_ ;
#endif
#ifdef NEED_AUXV
    auxv    = auxv_;
#endif

#ifdef USE_LIBC_START_MAIN
#ifdef __x86_64__
    asm volatile("xor  %%ecx, %%ecx\n"
                 "push %%rcx\n" /* align the stack */
                 "push %%rcx\n"
                 "push %%rcx\n"
                 "pop  %%r8\n"
                 "pop  %%r9\n"
                 /* 'call __libc_start_main' would go through the PLT, and we
                  * don't want to carry that bloat */
                 "call *__libc_start_main@GOTPCREL(%%rip)\n"
                 :
                 :"S" (argc), "D" (main), "d" (argv)
                 :);
#else
    __libc_start_main(main, (int)argc, argv, NULL, NULL, NULL, (void*)argv);
#endif

    __builtin_unreachable();
#else /* USE_LIBC_START_MAIN */
    register int rv = main((int)argc, argv, env_); // this part needs to be in C so
                                                   // LTO can do its inlining magic
#ifdef DESPERATE_UNSAFE
	SYS_break(); // just don't care about the return value, quit the program in one instruction
#else
	SYS_exit_group(rv);
#endif
#endif
}

#endif /* CUSTOM_CRT0 */

