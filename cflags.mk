
OPTFLAG ?= -O2
OPTNUMFLAG ?= -O2

ifneq ($(FORCE_LTO_OFF),1)
CFLAGS += -flto
ifneq ($(IS_CLANG),1)
CFLAGS += -fuse-linker-plugin
else
GCCFLAGS += -fuse-linker-plugin
endif
endif

# -Wl,-z,execstack -Wa,--execstack
CFLAGS += -I inc -I obj -g -fno-plt \
    \
    -fno-stack-protector -fno-stack-check -fno-unwind-tables \
    -fno-asynchronous-unwind-tables -fomit-frame-pointer -ffast-math \
    \
    -Wall -Wextra -Wno-attributes -Wno-unused-function -Wno-unknown-pragmas \
    -Wno-unused-parameter \
    -Werror=implicit-function-declaration \
    \
    -fno-pic -fno-PIE -fno-PIC

CCFLAGS := $(OPTFLAG)

ifeq ($(IS_CLANG),1)
CLANGFLAGS += -Weverything -Wno-language-extension-token -Wno-padded \
    -Wno-disabled-macro-expansion -Wno-gnu-statement-expression \
    -Wno-keyword-macro -Wno-reserved-id-macro -Wno-documentation \
    -Wno-empty-translation-unit -Wno-static-in-inline -Wno-undefined-inline
GCCFLAGS += -no-pie
else
CFLAGS += -no-pie
endif

CXXFLAGS += -fno-exceptions -fno-rtti -fno-threadsafe-statics

LIBS += -lGL -lm

ifneq ($(IS_CLANG),1)
LDFLAGS += -Wl,--build-id=none -z norelro -Wl,-z,noseparate-code
else
GCCLDFLAGS += -Wl,--build-id=none -z norelro -Wl,-z,noseparate-code
endif
LDFLAGS += -Wl,--no-eh-frame-hdr -Wl,--no-ld-generated-unwind-info $(OPTNUMFLAG) #\
    -Wl,-z,undefs -Wl,--unresolved-symbols=ignore-all # this can haunt you when misused
LDFLAGS += -T ld/smaller64.ld -Wl,--hash-style=sysv -Wl,-z,nodynamic-undefined-weak

SMOLPYOPT := -d
SMOLRTOPT := -DUSE_INTERP -DUSE_DNLOAD_LOADER
ifeq ($(BACKEND_GTK),1)
SMOLRTOPT += -DSKIP_ZERO_VALUE
endif
#-DUSE_NX #-DUSE_DL_FINI #-DALIGN_STACK #-DUSE_NX #-DUSE_DL_FINI
#-DUSE_DT_DEBUG #-DSKIP_ENTIRES #-DUSE_DNLOAD_LOADER #-DNO_START_ARG
SMOLLDOPT := -nostartfiles --oformat=binary

ifeq ($(CUSTOM_CRT0),1)
ENTRYPOINT := _start
else
ENTRYPOINT := main
endif

INCLINKOPT := -nostdlib -Wl,--entry -Wl,$(ENTRYPOINT) \
    -Wl,--gc-sections -Wl,--print-gc-sections
ifeq ($(IS_GCC_9),1)
INCLINKOPT += -flinker-output=nolto-rel
endif

# ---

CFLAGS += -m64 -march=core2
NASMFLAGS += -f elf64 -g -F dwarf

ifneq ($(FORCE_LTO_OFF),1)
CFLAGS += -ffunction-sections -fdata-sections #-fwhole-program
LDFLAGS += -Wl,--gc-sections
endif

# ---

ifeq ($(CUSTOM_CRT0),1)
LDFLAGS += -nostartfiles
endif

ifeq ($(BACKEND_GTK),1)
CFLAGS += $(shell $(PKGCONFIG) --cflags gtk+-3.0)
# not pkgconfig --libs here, because it gives too many of them
LIBS += -lgobject-2.0 -lgtk-3 -lgdk-3
endif

ifeq ($(BACKEND_SDL2),1)
CFLAGS += $(shell $(SDLCONFIG) --cflags)
# no -pthread please
LIBS += -lSDL2
endif

CFLAGS += $(shell $(PKGCONFIG) --cflags librsvg-2.0)
LIBS   += $(shell $(PKGCONFIG) --libs   librsvg-2.0)

# ---

LDFLAGS := -Wl,--as-needed $(LIBS) $(LDFLAGS)

# ---

ifeq ($(NEED_ENVIRON),1)
CFLAGS += -DNEED_ENVIRON
endif
ifeq ($(CUSTOM_CRT0),1)
CFLAGS += -DCUSTOM_CRT0
endif
ifeq ($(NEED_AUXV),1)
CFLAGS += -DNEED_AUXV
endif

-include cflags-config.mk
-include ../cflags-config.mk

CXXFLAGS := $(CFLAGS) $(CCFLAGS) $(CXXFLAGS)

