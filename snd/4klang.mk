
obj/4klang.%: mus/4klang.% xlatsection.awk obj/
	$(AWK) -f "xlatsection.awk" "$<" > "$@"
obj/4klang.h: mus/4klang.h obj/
	$(SED) -E 's/extern "C"/EXTERN_C/' "$<" > "$@"

obj/4klang.o: obj/4klang.asm obj/4klang.inc
	$(NASM) -g -I obj -f elf32 -o "$@" "$<"

DFILES_H := obj/4klang.h
EXTRAOBJS := obj/4klang.o

