
#ifndef K2_SND_SYNTH_H_
#define K2_SND_SYNTH_H_

#include <stddef.h>
#include <stdint.h>

#include "def.h"

#define SYNTH_FOURCC(s) ((s)[0]|((s)[1]<<8)|((s)[2]<<16)|((s)[3]<<24))

#define SYNTH_WAVE_RIFF_SIZE(data_size) (36+(data_size))
#define SYNTH_WAVE_FMT_SIZE (0x10)

#define SYNTH_RIFF_MAGIC SYNTH_FOURCC("RIFF")
#define SYNTH_WAVE_MAGIC SYNTH_FOURCC("WAVE")
#define SYNTH_FMT_MAGIC  SYNTH_FOURCC("fmt ")
#define SYNTH_DATA_MAGIC SYNTH_FOURCC("data")

struct synth_wave_hdr {
	uint32_t riff_magic;
	uint32_t riff_size ;
	uint32_t wave_magic;
	uint32_t fmt_magic ;
	uint32_t fmt_size  ;

	uint16_t format     ;
	uint16_t channels   ;
	uint32_t sample_rate;
	uint32_t byte_rate  ;
	uint16_t alignment  ;
	uint16_t bit_depth  ;

	uint32_t data_magic;
	uint32_t data_size ;
};

__attribute__((__noreturn__))
lto_inline void synth_execve(void);
lto_inline void synth_init(int pipefd);
lto_inline void synth_main(int pipefd);
lto_inline void synth_write_wavhdr(int pipefd);
lto_inline pure_fn size_t synth_get_outbuf_size(void);
lto_inline pure_fn const void* synth_get_outbuf(void);
lto_inline pure_fn const struct synth_wave_hdr* synth_get_wavhdr(void);

#endif

