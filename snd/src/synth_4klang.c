
#ifdef SYNTH_BACKEND_4KLANG

#include <stddef.h>

#include "def.h"
#include "sys.h"

// 4klang header file generation workarounds
#define __stdcall
#define EXTERN_C

#include "4klang.h"

#ifdef FLOAT_32BIT
#define APLAY_SAMPLE_FORMAT "FLOAT_LE"
#else
/*
 * If you encounter this error, you should look at your 4klang.h file and check
 * the line above the "#define SAMPLE_TYPE" one. This macro definition can be
 * used to give the APLAY_SAMPLE_FORMAT the correct string value. Consult the
 * aplay(1) manpage for the details. For example, signed 16-bit int is S16_LE.
 */
#error "This 4klang sample type isn't \"supported\" yet."
#endif

// ---

#include "synth.h"

static SAMPLE_TYPE output_buffer[MAX_SAMPLES*2];

static const char*const argv_aplay[] = {
	"/usr/bin/aplay",
		"-c2",
		"-f" APLAY_SAMPLE_FORMAT,
		"-r" STRINGIFY(SAMPLE_RATE),
	NULL
};

void synth_execve() {
	SYS_execvec(
		argv_aplay[0], argv_aplay, &argv_aplay[4] // null string
	);
}

void synth_init(int _) {
	_4klang_render(output_buffer);
}

void synth_main(int pipefd) {
	//ssize_t writesz = 65536 /* pipe buffer size */, readsz;
	//const char* outbuf = (const char*)output_buffer;
	//do {
	//	readsz = SYS_write(pipefd, outbuf, writesz);
	//	outbuf += writesz;
	//} while (readsz == writesz);

	SYS_write(pipefd, (const char*)output_buffer, sizeof(output_buffer));
}

size_t synth_get_outbuf_size() {
	return sizeof(output_buffer);
}
const void* synth_get_outbuf() {
	return output_buffer;
}

static const struct synth_wave_hdr wavhdr = {
	.riff_magic = SYNTH_RIFF_MAGIC,
	.riff_size  = SYNTH_WAVE_RIFF_SIZE(sizeof(output_buffer)),
	.wave_magic = SYNTH_WAVE_MAGIC,
	.fmt_magic  = SYNTH_FMT_MAGIC,
	.fmt_size   = SYNTH_WAVE_FMT_SIZE,

#ifdef FLOAT_32BIT
	.format = 3, // floating-point PCM
#else
	.format = 1, // integer PCM
#endif
	.channels = 2,
	.sample_rate = SAMPLE_RATE,
	.byte_rate = sizeof(SAMPLE_TYPE)*SAMPLE_RATE*2,
	.alignment = 4,
	.bit_depth = ((sizeof(SAMPLE_TYPE)*8)<<16),

	.data_magic = SYNTH_DATA_MAGIC,
	.data_size  = sizeof(output_buffer)
};

const struct synth_wave_hdr* synth_get_wavhdr() {
	return wavhdr;
}

void synth_write_wavhdr(int pipefd) {
	SYS_write(pipefd, wavehdr, sizeof(wavehdr));
}

#endif /* SYNTH_BACKEND_4KLANG */

