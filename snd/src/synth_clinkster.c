
#ifdef SYNTH_BACKEND_CLINKSTER

#include <stddef.h>

#include "def.h"
#include "sys.h"

#include "clinkster.h"

#include "synth.h"

static const char*const argv_aplay[] = {
	"/usr/bin/aplay",
	NULL
};

void synth_execve() {
	SYS_execvec(
		argv_aplay[0], argv_aplay, &argv_aplay[1] // null string
	);
}

void synth_init(int _) {
	Clinkster_GenerateMusic();
}

void synth_main(int pipefd) {
#ifndef MODE_DUMP
	SYS_write(pipefd, Clinkster_WavFileHeader, sizeof(Clinkster_WavFileHeader));
#endif
	SYS_write(pipefd, Clinkster_MusicBuffer, Clinkster_WavFileHeader[10]);
}

size_t synth_get_outbuf_size() {
	return Clinkster_WavFileHeader[10];
}
const void* synth_get_outbuf() {
	return Clinkster_MusicBuffer;
}

const struct synth_wave_hdr* synth_get_wavhdr() {
	return (const struct synth_wave_hdr*)Clinkster_WavFileHeader;
}
void synth_write_wavhdr(int pipefd) {
	SYS_write(pipefd, Clinkster_WavFileHeader, sizeof(Clinkster_WavFileHeader));
}

#endif /* SYNTH_BACKEND_CLINKSTER */

