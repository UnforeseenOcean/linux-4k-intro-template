
#if (!(defined(__i386__) && defined(USE_INLINE_ASM_STUFF) && !defined(MODE_DUMP)) || 1)

#include <stdint.h>

#include "dbg.h"
#include "sys.h"

#include "synth.h"

#ifdef SND_USE_CRT0
int main(int argc, char* argv[], char* envp[]) {
#else
#ifdef __clang__
extern void _start(void); // sigh
#endif
__attribute__((__used__, __noreturn__, __externally_visible__
#ifndef __clang__
	,__naked__
#endif
))
void _start(void) {
#ifdef __i386__
	//asm volatile("andl $-16, %%esp\n":::); // align stack 'cause blargh
#endif
#endif

#ifdef MODE_DUMP
	synth_init(STDERR_FILENO);
	synth_write_wavhdr(STDERR_FILENO);
	synth_main(STDERR_FILENO);
#else /* MODE_DUMP */
	// TODO: figure out how to make the array itself const (i.e. make it
	//       reside in .rodata), then place it back outside of this function
	int aupipe[2];
	SYS_pipe(aupipe);

	if (!SYS_fork()) {
		// child
#ifndef DESPERATE
		SYS_close(aupipe[1]); // close write end
#endif
		SYS_dup2(aupipe[0], STDIN_FILENO);

		synth_execve();
		__builtin_unreachable();
	} else {
		// parent
#ifndef DESPERATE
		SYS_close(aupipe[0]); // close read end
#endif
		//SYS_dup2(aupipe[1], STDOUT_FILENO);
	}
	int aufd = aupipe[1];

	synth_init(aufd);

	uint32_t msg[PIPE_WILL_BLOCK_BUF/sizeof(uint32_t)];
#ifndef DESPERATE
	msg[0] = 'K' | ('2' << 8) | ('K' << 16) | ('2' << 24);
#endif
#ifndef WEAK_VA_SYNC
	SYS_write(STDOUT_FILENO, (const void*)&msg, PIPE_WILL_BLOCK_BUF);
	SYS_close(STDOUT_FILENO);
#endif
#ifndef WEAK_AV_SYNC
	SYS_read(STDIN_FILENO, &msg, PIPE_WILL_BLOCK_BUF);
#endif

	synth_main(aufd);

#ifndef DESPERATE
	SYS_close(aufd);
#endif

#ifndef DESPERATE_UNSAFE
	SYS_waitpid(0,NULL,0);
#ifdef SND_USE_CRT0
	return DONT_CARE(int);
#else
	SYS_exit(DONT_CARE(int));
#endif
#else
	SYS_break(); __builtin_unreachable();
#endif
#endif /* !MODE_DUMP */
}

#endif

