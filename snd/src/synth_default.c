
#include "dbg.h"
#include "sys.h"

#include "synth.h"

__attribute__((__weak__))
void synth_execve() { SYS_break(); /*__builtin_unreachable();*/ }

__attribute__((__weak__))
void synth_init(int _) {
#ifndef NO_ERROR_CHECKING
	k2pingimp("no synth backend\n");
#endif
}

__attribute__((__weak__))
void synth_main(int _) { }

__attribute__((__weak__))
size_t synth_get_outbuf_size() { return 0; }
__attribute__((__weak__))
const void* synth_get_outbuf() { return NULL; }

__attribute__((__weak__))
const struct synth_wave_hdr* synth_get_wavhdr() { return NULL; }

__attribute__((__weak__))
void synth_write_wavhdr(int _) { }

