
#if defined(__i386__) && defined(USE_INLINE_ASM_STUFF) && !defined(MODE_DUMP) && 0 /* currently no worky */

#include <stdint.h>

#include "sys.h"

#include "synth.h"

#ifdef SND_USE_CRT0
int main(int argc, char* argv[], char* envp[]) {
#else
#ifdef __clang__
extern void _start(void); // sigh
#endif
__attribute__((__used__, __noreturn__, __externally_visible__
#ifndef __clang__
	,__naked__
#endif
))
void _start(void) {
#endif
// b c d S D
	register int childpid asm("eax"), aufd asm("ebx");
	asm volatile(
		"push $0; push $0; push %%esp; pop %%ebp\n" // pipefd storage

		"push $" STRINGIFY(SYS_NR_fork) "\n"

		"push %%ebp; push $" STRINGIFY(SYS_NR_pipe) "\n"
		"pop %%eax; pop %%ebx\n"
		"int $0x80\n"

		// fork
		"pop %%eax\n"
		"int $0x80\n"
		"movl 4(%%ebp), %%ebx\n"
	:"=a"(childpid),"=b"(aufd)::);

	if (!childpid) {
		// child
		asm volatile(
			"push $" STRINGIFY(STDIN_FILENO) "; push (%%ebp)\n"
			"push $" STRINGIFY(SYS_NR_dup2) "\n"
			"pop %%eax; pop %%ebx; pop %%ecx\n"
			"int $0x80\n"
		:::);
		synth_execve();
		__builtin_unreachable();
	}

	synth_init(aufd);

#ifndef DESPERATE_UNSAFE
	asm volatile(
		"push $0; push $0; push $0; push $" STRINGIFY(SYS_NR_waitpid) "\n"
	:::);
#endif

#if !defined(WEAK_VA_SYNC) || !defined(WEAK_AV_SYNC)
#if !defined(WEAK_VA_SYNC) && defined(WEAK_AV_SYNC)
	asm volatile(
		"push %%esp; push $65540; push $1; push $" STRINGIFY(SYS_NR_write)"\n"
		"pop %%eax; pop %%ebx; pop %%edx; pop %%ecx\n"
		"int $0x80\n"
	:::"ebx");
#else
	asm volatile(
			//"subl $65540, %%esp\n"
			"push %%esp; pop %%ecx\n"

#ifdef WEAK_VA_SYNC
			"push $65540\n"
#endif
			"push $0; push $" STRINGIFY(SYS_NR_read) "\n"
#ifndef WEAK_VA_SYNC
			"push $65540; push $1; push $" STRINGIFY(SYS_NR_write) "\n"
			// write
			"pop %%eax; pop %%ebx; pop %%edx\n"
			"int $0x80\n"
#endif
			// read
			"pop %%eax; pop %%ebx\n"
#ifdef WEAK_VA_SYNC
			"pop %%edx\n"
#endif
			"int $0x80\n"

			//"addl $65540, %%esp\n"
	:::"ebx");
#endif
#endif

	synth_main(aufd);

#ifndef SND_USE_CRT0
	asm volatile(
		"push $" STRINGIFY(SYS_NR_exit) "\n"
	:::);
#endif

#ifndef DESPERATE_UNSAFE
	asm volatile(
		// waitpid
		"pop %%eax; pop %%ebx; pop %%ecx; pop %%edx\n"
		"int $0x80\n"
	:::);
#endif

#ifdef SND_USE_CRT0
	return DONT_CARE(int);
#else
	asm volatile(
		"pop %%eax\n"
		"int $0x80\n"
	:::);
#endif
}

#endif

