
CLINKSTERPROJS := $(wildcard mus/*.clinkster.xrns)

CFLAGS += -I ../ext/Clinkster/player/

obj/%.xrns.asm: mus/%.xrns obj/
	$(PYTHON2) ../ext/Clinkster/converter/renoiseconvert.py "$<" "$@"
obj/%.xrns.o: obj/%.xrns.asm ../ext/Clinkster/player/clinkster.asm
	@cp "$<" "obj/music.asm"
	$(NASM) -DLINUX -felf32 -I obj -I ../ext/Clinkster/player/ -o "$@" "../ext/Clinkster/player/clinkster.asm"
	@$(RM) "obj/music.asm"

EXTRAOBJS := $(patsubst mus/%.xrns,obj/%.xrns.o,$(CLINKSTERPROJS))

.PRECIOUS: $(EXTRAOBJS) $(patsubst mus/%.xrns,obj/%.xrns.asm,$(CLINKSTERPROJS))

