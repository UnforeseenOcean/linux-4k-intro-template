#
# De-windowsify section and exported symbol names in 4klang asm files
#

{
	if ($0 ~ "^section[\t ]+(\.[^\t ]*)[\t ]+([^\t ]*)[\t ]+align=(.*)$") {
		basesect = $3
		if (basesect == "code") {
			basesect = "text"
		}

		extra = ""

		if (basesect == "text") {
			extra = " progbits alloc exec nowrite "
		} else if (basesect == "data") {
			extra = " progbits alloc noexec write "
		} else if (basesect == "bss") {
			extra = " nobits alloc noexec write "
		}

		print $1 " ." basesect $2 extra $4
	} else if ($2 ~ "_4klang_render@4" && $1 == "export_func") {
		print $1 " 4klang_render"
	} else {
		print $0
	}
}

