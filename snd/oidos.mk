
OIDOSPROJS := $(wildcard mus/*.oidos.xrns)

CFLAGS += -I ../ext/oidos/player/
NASMFLAGS_ := -DLINUX -felf32 -I obj/ -I ../ext/oidos/player/

obj/oidos-random.o: ../ext/oidos/player/random.asm
	$(NASM) $(NASMFLAGS_) -o "$@" "$<"
obj/%.xrns.asm: mus/%.xrns obj/
	$(PYTHON2) ../ext/oidos/convert/oidosconvert.py "$<" "$@"
obj/%.xrns.o: obj/%.xrns.asm ../ext/oidos/player/oidos.asm
	@cp "$<" "obj/music.asm"
	$(NASM) $(NASMFLAGS_) -o "$@" "../ext/oidos/player/oidos.asm"
	@$(RM) "obj/music.asm"

EXTRAOBJS := $(patsubst mus/%.xrns,obj/%.xrns.o,$(OIDOSPROJS)) obj/oidos-random.o

.PRECIOUS: $(EXTRAOBJS) $(patsubst mus/%.xrns,obj/%.xrns.asm,$(OIDOSPROJS))

