
# -fno-plt -fno-pie -no-pie kills the .got.plt

OPTFLAG ?= -O2
OPTNUMFLAG ?= -O2

CFLAGS += -I inc -I ../inc -I obj -g -flto \
    -fno-plt -fno-pie \
    \
    -fno-stack-protector -fno-stack-check -fno-unwind-tables \
    -fno-asynchronous-unwind-tables -fomit-frame-pointer \
    \
    -Wall -Wextra -Wno-attributes -Wno-unused-function -Wno-unknown-pragmas \
    -Wno-unused-parameter \
    -Werror=implicit-function-declaration \
    -DNOSTDLIB

CFLAGS += -ffunction-sections -fdata-sections

CCFLAGS += $(OPTFLAG)

ifneq ($(IS_CLANG),1)
CFLAGS += -fuse-linker-plugin -no-pie
CFLAGS += -Wl,--build-id=none -Wl,-z,norelro -Wl,-z,noseparate-code
else
CLANGFLAGS += -Weverything -Wno-language-extension-token -Wno-padded \
    -Wno-disabled-macro-expansion -Wno-gnu-statement-expression \
    -Wno-keyword-macro -Wno-reserved-id-macro -Wno-documentation \
    -Wno-empty-translation-unit -Wno-static-in-inline -Wno-undefined-inline
GCCFLAGS += -fuse-linker-plugin -no-pie
GCCFLAGS += -Wl,--build-id=none -Wl,-z,norelro -Wl,-z,noseparate-code
endif

#LIBS +=
LDFLAGS += -static -nostdlib -nostartfiles $(OPTNUMFLAG)

# ---

CFLAGS += -m$(BITS) -march=core2

# ---

# LDFLAGS, LIBS ifv config

# ---

LDFLAGS := $(LIBS) $(LDFLAGS)
LDFLAGS := -T ld/ld32.ld $(LDFLAGS)

# ---

ENTRYPOINT := _start
INCLINKOPT := -nostdlib -Wl,--entry -Wl,$(ENTRYPOINT) \
    -Wl,--gc-sections -Wl,--print-gc-sections
ifeq ($(IS_GCC_9),1)
INCLINKOPT += -flinker-output=nolto-rel
endif

include ../cflags-config.mk

