
# K2 Linux introsystem (barebones version)

## Requirements

* gcc, optionally clang (new enough, mine are 8.2.1 and 7.0.1, but gcc &gt;=6 and
  clang &gt;=5 should be ok) GCC is always required (for linker scripts), but
  clang can be used, too, to compile the actual C code.
* python 3, for smol, the dynamic linker, and the dnload GLSL minifier (optional)
* python 2, for the Oidos/Clinkster Renoise converter scripts
* scanelf from pax-utils, for smol
* mono (and fsharpc) for LLB's Shader Minifier (optional)
* yasm (NOT nasm), for norjohe, the optimizing static linker, and for smol.
* binutils, make (Possibly GNU Make-only, but not confirmed), awk, sed, ...
* Linux >=3.19, OpenGL
* GTK3 development headers (optional, for the GTK3 backend)
* SDL2 development headers (optional, for the SDL2 backend)
* other boring, basic stuff

Some special code is needed (eg. norjohe), this is included as a git submodule
and is compiled when make is run for the first time.


Running the final intro should only require the base packages of Ubuntu 19.04:
http://releases.ubuntu.com/19.04/ubuntu-19.04-desktop-amd64.manifest . The
SDL2 backend requires extra libraries, but it is optional.

## Manual

If you just want to play with a fullscreen shader, edit `shdr/intro2.glsl`. The
exported synth files can be put in the `snd/mus/` subdirectory.

Most of the actual CPU code you want to write/modify is in the `demo`
subdirectory. The `src` and `snd` directories contain boilerplate for
creating a GL context and setting up a synth, you probably don't want to touch
that, unless it contains a bug, or you want to add extra (development)
functionality, or if you think you can squeeze out a few extra bytes.

In the `config.mk` file, you can select which GL backend and which synth to
use, as well whether verbose logging should be turned on, and the optimization
level. (Increasing that removes a few runtime checks and skips a few more
initialization steps, so these might be 'unsafe'.) You can also choose to run
the intro in fullscreen mode, hide the cursor, change the resolution, etc.

If you're ready, run `make` to build the project. (You might need to run
`make clean` first.) LLB's Shader Minifier, smol, vondehi, ... are all
automatically invoked to decrease the output size. If it finishes successfully,
you'll find a few files in the `bin` directory: `bin/main.{ld,smol}{,.vndh}`.
The `ld` ones are linked using the 'standard' GNU ld linker, while the `smol`
variants are, surprisingly, ran through smol. The latter ones are much smaller,
but the former is more stable and contains debug info etc. Files ending in
`.vndh` are compressed using vondehi. The `bin/main.smol.vndh` is probably the
one you want to submit for the compo.

## How it works

Everything is compiled using LTO so splitting the CPU code into multiple files
doesn't change much about the final size, while it keeps the code a bit more
manageable. "Standard' size-reducing compiler flags are used liberally, and
because smol is used, global variables from other libraries don't work yet.

After vondehi and smol have successfully decompressed and loaded the intro
code, a window and an OpenGL context is created. Shaders are compiled and bound
as usual, and then the program jumps into a tiny loop that fetches the current
time, then calls `glRecti` to render a fullscreen quad.

However, as the synth code is (usually, at least with 4klang, Oidos and
Clinkster) 32-bit only, and only the 64-bit versions of dynamic libraries are
installed, a few hacks need to be used to get those to work. After applying a
few patches at compiletime to make the synths work on Linux, the synth code and
data is then compiled and linked into a separate, static, extremely
stripped-down, 32-bit binary using norjohe. This binary is then embedded into
the main binary, which will fork before compiling the shaders, and the child
process will then execve the embedded sound binary, which then starts
precalcing the audio. To ensure that the visuals and audio remain synced, they
wait for each other by reading/writing in turn from a pipe, which defaults to
blocking IO. The actual sound output is done by piping to `aplay`.

