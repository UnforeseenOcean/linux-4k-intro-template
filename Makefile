
default: all

MAINDIR := $(shell pwd)

include config.mk
include platform.mk
include cflags.mk
include baserules.mk

dtn/snd: snd/bin/snd

demo/bin/demo.so: demo/
	$(MAKE) -C "$<"
demo/bin/demo_s.a: demo/
	$(MAKE) -C "$<"

snd/bin/snd: snd/
	$(MAKE) -C "$<"

ALIBS := demo/bin/demo_s.a

SPECIALS := snd/bin/snd $(ALIBS)

# this is the incremental linkage: create another object file, but with all
# the LTO etc. applied. we can then pass this optimized object to a less
# sophisticated (read: size-minimizing) linker
bin/main.inc.o: $(SPECIALS) $(CFILES_O) $(DFILES_O) #$(CXXFILES_O)
	$(CC) -r -o "$@" $(filter-out $(SPECIALS),$^) \
        -Wl,--whole-archive $(ALIBS) -Wl,--no-whole-archive \
        $(CFLAGS) $(INCLINKOPT) $(CLANGFLAGS) $(OPTNUMFLAG)
#        -T ld/relink.ld \
#	$(CXX) -o "$@" $^ $(CXXFLAGS) # .. etc

bin/%.ld: bin/%.inc.o
	$(GCC) -o "$@" "$<" $(CFLAGS) $(GCCFLAGS) $(LDFLAGS) $(GCCLDFLAGS) \
        $(DEFAULT_LIB_SEARCH_DIR_FLAGS) -Wl,-Map=$@.map -Wl,--cref

obj/%.ld.nover: bin/%.ld
	$(OBJCOPY) -R .gnu.version -R .gnu.version_r "$<" "$@"

bin/%.ld.stripped: obj/%.ld.nover
	$(NOELFVER) "$<" > "$@"
	$(STRIP) -s "$@"
	$(SSTRIP) -z "$@"
	chmod +x "$@"

obj/%.smol.syms.asm: bin/%.inc.o
	$(SMOL) $(SMOLPYOPT) $(LIBS) -lc $^ "$@"

obj/%.smol.stub.o: obj/%.smol.syms.asm
	$(NASM) $(NASMFLAGS) $(SMOLRTOPT) -I "$(SMOLSRC)" -o "$@" "$<"

bin/%.smol: obj/%.smol.stub.o bin/%.inc.o
	$(LD) $(SMOLLDOPT) -T $(SMOLLD)link.ld -Map=$@.map --cref -o "$@" $^

bin/%.ld.vndh: bin/%.ld.stripped
	$(AUTOVNDH) $(VNDH_FLAGS) "$<" > "$@"
	chmod +x "$@"

bin/%.smol.vndh: bin/%.smol
	$(AUTOVNDH) $(VNDH_FLAGS) "$<" > "$@"
	chmod +x "$@"

all: submoduledeps obj/ bin/ $(DFILES_H) $(DFILES_C)
all: bin/main.ld.vndh
ifneq ($(DISABLE_SMOL),1)
all: bin/main.smol.vndh
endif

t: all
	bin/main.ld

clean:
	$(MAKE) -C snd clean
	$(MAKE) -C demo clean
	@rm -vf obj/* bin/* dtn/*_glsl

todo:
	@rg -g '!ext/*' -piF TODO -C2 --no-hidden | less -RS

video.mp4 audio.wav: all
	bash -c 'bin/main.ld > >(ffmpeg -f rawvideo -s:v $(CANVAS_WIDTH)x$(CANVAS_HEIGHT) -pix_fmt rgb24 -r 60 -i - -vf vflip video.mp4) 2>audio.wav'

demo.mp4: audio.wav #video.mp4
	ffmpeg -i video.mp4 -i audio.wav -c:v copy "$@"

dump-demo: demo.mp4

.PHONY: all default clean t todo dump-demo $(ALIBS)

.PRECIOUS: bin/main.smol bin/main.ld.stripped bin/main.ld \
    obj/main.smol.syms.asm obj/main.smol.stub.o obj/main.ld.nover \
    dtn/frag_glsl dtn/vert_glsl dtn/intro_glsl dtn/intro2_glsl

