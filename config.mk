
# mostly for debugging the introsystem and toolchain
#FORCE_LTO_OFF := 1

MODE_DUMP ?= 0

# only let the main binary wait for the audio thread to finish precalcing
WEAK_AV_SYNC ?= 1
# only let the audio thread wait for the main binary to finish precalcing
#WEAK_VA_SYNC ?= 1

# uncomment if you want GCC instead
#CC := clang
# -O3 -Os -Oz ...
OPTFLAG := -Os
# only numbered ones
OPTNUMFLAG := -O2

CUSTOM_CRT0         ?= 1

DISABLE_SMOL ?= 0

# if CUSTOM_CRT0
USE_LIBC_START_MAIN ?= 1
SND_USE_CRT0 ?= 0

# if USE_LIBC_START_MAIN==0:
# ---
# you probably want this (eg. if you're planning to do X
# or to call fork(2)/execve(2))
NEED_ENVIRON ?= 1
#NEED_AUXV    ?= 1
# ---

# disable this one if something is breaking. maybe enable 'verbose' as well
ifeq ($(MODE_DUMP),1)
NO_ERROR_CHECKING ?= 0
else
NO_ERROR_CHECKING ?= 1
endif
DESPERATE ?= 1
#DESPERATE_UNSAFE ?= 1

# 2309
#BACKEND_GTK ?= 1
# 2102
BACKEND_SDL2 ?= 1

#SYNTH_BACKEND_4KLANG ?= 1
#SYNTH_BACKEND_OIDOS ?= 1
SYNTH_BACKEND_CLINKSTER ?= 1

CANVAS_WIDTH  ?= 1920
CANVAS_HEIGHT ?= 1080

ifeq ($(MODE_DUMP),1)
NO_AUDIO ?= 1
else
NO_AUDIO ?= 0
endif

#CANVAS_RESIZABLE ?= 1

FULLSCREEN ?= 0
HIDEMOUSE  ?= 0
VSYNC      ?= 0

# verbose logging mode
VERBOSE ?= 0

ifeq ($(DESPERATE),1)
USE_INLINE_ASM_STUFF ?= 1
endif
USE_INLINE_ASM_STUFF ?= 0

# XXX: intel gpus NEED the core profile!
ifeq ($(BACKEND_GTK),1)
USE_CORE_PROFILE ?= 1
else
#USE_CORE_PROFILE ?= 0
endif

# if you're doing a 64k, add -x
VNDH_FLAGS := -g -l -v --vndh $(MAINDIR)/ext/vondehi

ifeq ($(WEAK_AV_SYNC),1)
ifeq ($(WEAK_VA_SYNC),1)
$(error "WEAK_AV_SYNC and WEAK_VA_SYNC can't be enabled at the same time.")
endif
endif

ifneq ($(NO_AUDIO),1)
ifneq ($(SYNTH_BACKEND_4KLANG),1)
ifneq ($(SYNTH_BACKEND_OIDOS),1)
ifneq ($(SYNTH_BACKEND_CLINKSTER),1)
$(warning "NO_AUDIO set to zero while no synth backend is selected! setting NO_AUDIO to 1...")
NO_AUDIO := 1
endif
endif
endif
endif

