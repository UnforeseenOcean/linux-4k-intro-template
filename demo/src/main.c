
// sorry, las
#include <librsvg/rsvg.h>

#include "file.h"
#include "shdr.h"
#include "dbg.h"
#include "sys.h"

#include "demo.h"

#include "intro2_glsl.bin.h"
#ifdef USE_CORE_PROFILE
#include "vert_glsl.bin.h"
#endif
#include "map_svg.bin.h"

#ifndef DESPERATE
static GLuint prgm;
#ifdef USE_CORE_PROFILE
static GLuint va;
#endif
#endif
static GLint rloc, tloc;

void effect_init() {
#ifdef DESPERATE
	GLuint prgm; // on the stack instead of .bss
#ifdef USE_CORE_PROFILE
	GLuint va;
#endif
#endif

	struct file_res fragd = file_read(intro2_glsl)
#ifdef USE_CORE_PROFILE
				  , vertd = file_read(vert_glsl)
#endif
				  ;

#ifdef USE_CORE_PROFILE
	GLuint f = shdr_mk_frag((const char*)fragd.data),
		   v = shdr_mk_vert((const char*)vertd.data);

	prgm = prgm_mk2(f, v);

#ifndef DESPERATE
	glDeleteShader(f);
	glDeleteShader(v);
#endif
#else
	prgm = shdrprgm_mk_frag((const char*)fragd.data);
#endif

	glUseProgram(prgm);

	// README: here you can do custom init stuff (eg. create textures)
	// (exercise for the reader: convert this to asm)

	rloc = glGetUniformLocation(prgm, "R");
	tloc = glGetUniformLocation(prgm, "T");

	GLuint svgtex;
	struct file_res svgmap = file_read(map_svg);
	GError* err = NULL;
	RsvgHandle* rh = rsvg_handle_new_from_data(svgmap.data, svgmap.len, &err);
	const uint8_t* pxdata = gdk_pixbuf_read_pixels(rsvg_handle_get_pixbuf(rh));

	glGenTextures(1, &svgtex);
	glBindTexture(GL_TEXTURE_2D, svgtex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024,1024, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, pxdata);
#if 1
	glTexParameteri(GL_TEXTURE_2D/*_ARRAY*/, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
#endif

#ifdef USE_CORE_PROFILE
	glGenVertexArrays(1, &va);
#endif

#ifdef USE_CORE_PROFILE
	glBindVertexArray(va);
	glVertexAttrib1f(0,0);
#endif

#ifndef DESPERATE
	glClearColor(0,0,0,1);
#endif
}

void effect_tick(demotime_t t, int w, int h) {
#ifndef DESPERATE
	glClear(GL_COLOR_BUFFER_BIT);
#endif

#ifdef USE_CORE_PROFILE
	glUniform1f(tloc, (float)t);
	glUniform2i(rloc, w, h);
	glViewport(0,0,w,h);
#if defined(__x86_64__) && !defined(MODE_DUMP) && defined(USE_INLINE_ASM_STUFF)
	asm volatile("push $4; push $0; push $" STRINGIFY(GL_TRIANGLE_STRIP) "\n"
				 "pop %%rdi; pop %%rsi; pop %%rdx\n"
				 "call *glDrawArrays@GOTPCREL(%%rip)\n"
				 :::"rax","rdi","rsi","rdx","rcx","r8","r9","r10","r11");
#else
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
#endif /* x86_64 && MODE_REL */
#else /* !USE_CORE_PROFILE */
	glUniform1f(tloc, (float)t);
#if defined(__x86_64__) && !defined(MODE_DUMP) && defined(USE_INLINE_ASM_STUFF)
	asm volatile("push $1; push $1; push $-1; push $-1\n"
				 "push $0; push $0; push %[w]; push %[h]\n"
				 "push %[w]; push %[h]; push $0\n"
				 /*"push $1\n"
				 "pop %%rdi\n"
				 "call *glUniform1f@GOTPCREL(%%rip)\n"*/
				 "pop %%rdi; pop %%rdx; pop %%rsi\n"
				 "call *glUniform2i@GOTPCREL(%%rip)\n"
				 "pop %%rcx; pop %%rdx; pop %%rsi; pop %%rdi\n"
				 "call *glViewport@GOTPCREL(%%rip)\n"
				 "pop %%rdi; pop %%rsi; pop %%rdx; pop %%rcx\n"
				 "call *glRecti@GOTPCREL(%%rip)\n"
				 ::[w]"r"((size_t)w),[h]"r"((size_t)h)//,[t]"S"(t)
				 :"rax","rdi","rsi","rdx","rcx","r8","r9","r10","r11");
#else /* x86_64 && MODE_REL */
	glUniform2i(rloc, w, h);
	glViewport(0,0,w,h);
	glRecti(-1,-1,1,1);
#endif /* asm stuff */
#endif /* USE_CORE_PROFILE */
}

#if !defined(DESPERATE)
void effect_kill() {
	glDeleteProgram(prgm);
#ifdef USE_CORE_PROFILE
	glDeleteVertexArrays(1, &va);
#endif
}
#endif

