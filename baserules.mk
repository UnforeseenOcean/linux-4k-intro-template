
GCC     ?= gcc
PYTHON2 ?= python2
PYTHON3 ?= python3
NASM    ?= nasm
AWK     ?= awk
SED     ?= sed
MONO    ?= mono

OBJCOPY ?= objcopy
STRIP   ?= strip

ELFKICKERS := $(MAINDIR)/ext/ELFkickers

ELFTOC := $(ELFKICKERS)/elftoc/elftoc
SSTRIP := $(ELFKICKERS)/sstrip/sstrip
OBJRES := $(ELFKICKERS)/objres/objres

AUTOVNDH := $(MAINDIR)/tool/autovndh.py
NRJH     := NRJH_ELFTOC=$(ELFTOC) $(MAINDIR)/ext/nrjh-inst/bin/norjohe
NOELFVER := $(MAINDIR)/ext/noelfver/noelfver

SMOL    := $(PYTHON3) $(MAINDIR)/ext/smol/src/smol.py
SMOLSRC := $(MAINDIR)/ext/smol/rt/
SMOLLD  := $(MAINDIR)/ext/smol/ld/

SHMIN_LLB_EXE := $(MAINDIR)/ext/shmin/shader_minifier.exe

SHMIN_LLB_FLAGS := --preserve-externals --format none

CFILES    := $(wildcard src/*.c)
CXXFILES  := $(wildcard src/*.cpp)
DFILES    := $(wildcard dtn/*)
GLSLFILES := $(wildcard shdr/*.glsl)
GLSLFILES_ := $(GLSLFILES) $(wildcard ../shdr/*.glsl)

DFILES += $(patsubst shdr/%.glsl,dtn/%_glsl,$(GLSLFILES))
DFILES_ := $(DFILES) $(patsubst ../shdr/%.glsl,../dtn/%_glsl,$(GLSLFILES_))

CFILES_O   := $(patsubst src/%.c,obj/%.c.o,$(CFILES))
CXXFILES_O := $(patsubst src/%.cpp,obj/%.cpp.o,$(CXXFILES))
DFILES_O   := $(patsubst dtn/%,obj/%.bin.o,$(DFILES))
DFILES_C   := $(patsubst dtn/%,obj/%.bin.c,$(DFILES))
DFILES_H   := $(patsubst dtn/%,obj/%.bin.h,$(DFILES_))

CC := $(CC) $(ARCH_CC_OPT)
LD := $(LD) $(ARCH_LD_OPT)

ifeq ($(DEFAULT_LIB_SEARCH_DIRS),)
DEFAULT_LIB_SEARCH_DIRS := $(shell $(CC) -print-search-dirs | tail +3 | head -1 | tail -c +13 | tr ':' ' ')
DEFAULT_LIB_SEARCH_DIR_FLAGS := $(foreach dir,$(DEFAULT_LIB_SEARCH_DIRS),-L$(dir))
#LDFLAGAS += $(DEFAULT_LIB_SEARCH_DIR_FLAGS)
endif

%/:
	@mkdir -vp "$@"

obj/%.bin.o: obj/%.bin.c obj/ $(DFILES_H)
	$(CC) -o "$@" -c "$<" $(CFLAGS) $(CLANGFLAGS) $(CCFLAGS)

obj/%.c.o: src/%.c obj/ $(DFILES_H)
	$(CC) -o "$@" -c "$<" $(CFLAGS) $(CLANGFLAGS) $(CCFLAGS)

obj/%.cpp.o: src/%.cpp obj/ $(DFILES_H)
	$(CXX) -o "$@" -c "$<" $(CXXFLAGS) $(CLANGFLAGS)

# TODO: cleanup
obj/%.bin.h: dtn/%
	printf "#ifndef HEADER_%s_\n#define HEADER_%s_\n__attribute__((__weak__))extern unsigned char %s[%d];\n#endif\n" \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        $(shell stat -Lc '%s' "$<") \
        > "$@"
../obj/%.bin.h: ../dtn/%
	printf "#ifndef HEADER_%s_\n#define HEADER_%s_\n__attribute__((__weak__))extern unsigned char %s[%d];\n#endif\n" \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        $(shell stat -Lc '%s' "$<") \
        > "$@"

obj/%.bin.c: dtn/% obj/%.bin.h
	@printf "extern unsigned char %s[];\n" \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        > "$@"
	xxd -i "$<" | head -n -1 >> "$@"
../obj/%.bin.c: ../dtn/% obj/%.bin.h
	@printf "extern unsigned char %s[];\n" \
        $(subst .,_,$(subst /,_,$(subst -,_,"$<"))) \
        > "$@"
	xxd -i "$<" | head -n -1 >> "$@"

# TODO: make pluggable?
dtn/%_glsl: shdr/%.glsl $(SHMIN_LLB_EXE)
	$(MONO) "$(SHMIN_LLB_EXE)" $(SHMIN_LLB_FLAGS) -o "$@" "$<"
../dtn/%_glsl: ../shdr/%.glsl $(SHMIN_LLB_EXE)
	$(MONO) "$(SHMIN_LLB_EXE)" $(SHMIN_LLB_FLAGS) -o "$@" "$<"

$(ELFTOC) $(SSTRIP) $(OBJRES) $(NRJH) $(NOELFVER) $(SHMIN_LLB_EXE):
	$(MAKE) -C $(MAINDIR)/ext

submoduledeps: $(ELFTOC) $(SSTRIP) $(OBJRES) $(NRJH) $(NOELFVER) \
    $(SHMIN_LLB_EXE)

.PHONY: submoduledeps

